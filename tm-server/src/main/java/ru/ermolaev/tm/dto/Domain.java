package ru.ermolaev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
public final class Domain implements Serializable {

    @NotNull
    private List<TaskDTO> taskDTOS = new ArrayList<>();

    @NotNull
    private List<ProjectDTO> projectDTOS = new ArrayList<>();

    @NotNull
    private List<UserDTO> userDTOS = new ArrayList<>();

}
