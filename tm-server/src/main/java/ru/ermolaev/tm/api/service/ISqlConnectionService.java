package ru.ermolaev.tm.api.service;

import javax.persistence.EntityManager;

public interface ISqlConnectionService {

    EntityManager getEntityManager();

}
