package ru.ermolaev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

public final class CommandsCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Show application's commands.";
    }

    @Override
    public void execute() {
        @NotNull final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (@NotNull final AbstractCommand command: commands) System.out.println(command.commandName());
    }

}
