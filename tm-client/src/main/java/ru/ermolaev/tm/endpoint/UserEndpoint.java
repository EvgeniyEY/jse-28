package ru.ermolaev.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-01-26T11:27:20.295+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.ermolaev.ru/", name = "UserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface UserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserMiddleNameRequest", output = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserMiddleNameResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserMiddleName/Fault/Exception")})
    @RequestWrapper(localName = "updateUserMiddleName", targetNamespace = "http://endpoint.tm.ermolaev.ru/", className = "ru.ermolaev.tm.endpoint.UpdateUserMiddleName")
    @ResponseWrapper(localName = "updateUserMiddleNameResponse", targetNamespace = "http://endpoint.tm.ermolaev.ru/", className = "ru.ermolaev.tm.endpoint.UpdateUserMiddleNameResponse")
    public void updateUserMiddleName(
        @WebParam(name = "session", targetNamespace = "")
        ru.ermolaev.tm.endpoint.SessionDTO session,
        @WebParam(name = "newMiddleName", targetNamespace = "")
        java.lang.String newMiddleName
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.ermolaev.ru/UserEndpoint/findUserProfileRequest", output = "http://endpoint.tm.ermolaev.ru/UserEndpoint/findUserProfileResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.ermolaev.ru/UserEndpoint/findUserProfile/Fault/Exception")})
    @RequestWrapper(localName = "findUserProfile", targetNamespace = "http://endpoint.tm.ermolaev.ru/", className = "ru.ermolaev.tm.endpoint.FindUserProfile")
    @ResponseWrapper(localName = "findUserProfileResponse", targetNamespace = "http://endpoint.tm.ermolaev.ru/", className = "ru.ermolaev.tm.endpoint.FindUserProfileResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ermolaev.tm.endpoint.UserDTO findUserProfile(
        @WebParam(name = "session", targetNamespace = "")
        ru.ermolaev.tm.endpoint.SessionDTO session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserEmailRequest", output = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserEmailResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserEmail/Fault/Exception")})
    @RequestWrapper(localName = "updateUserEmail", targetNamespace = "http://endpoint.tm.ermolaev.ru/", className = "ru.ermolaev.tm.endpoint.UpdateUserEmail")
    @ResponseWrapper(localName = "updateUserEmailResponse", targetNamespace = "http://endpoint.tm.ermolaev.ru/", className = "ru.ermolaev.tm.endpoint.UpdateUserEmailResponse")
    public void updateUserEmail(
        @WebParam(name = "session", targetNamespace = "")
        ru.ermolaev.tm.endpoint.SessionDTO session,
        @WebParam(name = "newEmail", targetNamespace = "")
        java.lang.String newEmail
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserFirstNameRequest", output = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserFirstNameResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserFirstName/Fault/Exception")})
    @RequestWrapper(localName = "updateUserFirstName", targetNamespace = "http://endpoint.tm.ermolaev.ru/", className = "ru.ermolaev.tm.endpoint.UpdateUserFirstName")
    @ResponseWrapper(localName = "updateUserFirstNameResponse", targetNamespace = "http://endpoint.tm.ermolaev.ru/", className = "ru.ermolaev.tm.endpoint.UpdateUserFirstNameResponse")
    public void updateUserFirstName(
        @WebParam(name = "session", targetNamespace = "")
        ru.ermolaev.tm.endpoint.SessionDTO session,
        @WebParam(name = "newFirstName", targetNamespace = "")
        java.lang.String newFirstName
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserPasswordRequest", output = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserPasswordResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserPassword/Fault/Exception")})
    @RequestWrapper(localName = "updateUserPassword", targetNamespace = "http://endpoint.tm.ermolaev.ru/", className = "ru.ermolaev.tm.endpoint.UpdateUserPassword")
    @ResponseWrapper(localName = "updateUserPasswordResponse", targetNamespace = "http://endpoint.tm.ermolaev.ru/", className = "ru.ermolaev.tm.endpoint.UpdateUserPasswordResponse")
    public void updateUserPassword(
        @WebParam(name = "session", targetNamespace = "")
        ru.ermolaev.tm.endpoint.SessionDTO session,
        @WebParam(name = "newPassword", targetNamespace = "")
        java.lang.String newPassword
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserLastNameRequest", output = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserLastNameResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.ermolaev.ru/UserEndpoint/updateUserLastName/Fault/Exception")})
    @RequestWrapper(localName = "updateUserLastName", targetNamespace = "http://endpoint.tm.ermolaev.ru/", className = "ru.ermolaev.tm.endpoint.UpdateUserLastName")
    @ResponseWrapper(localName = "updateUserLastNameResponse", targetNamespace = "http://endpoint.tm.ermolaev.ru/", className = "ru.ermolaev.tm.endpoint.UpdateUserLastNameResponse")
    public void updateUserLastName(
        @WebParam(name = "session", targetNamespace = "")
        ru.ermolaev.tm.endpoint.SessionDTO session,
        @WebParam(name = "newLastName", targetNamespace = "")
        java.lang.String newLastName
    ) throws Exception_Exception;
}
